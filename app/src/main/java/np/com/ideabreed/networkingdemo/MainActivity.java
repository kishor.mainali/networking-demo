package np.com.ideabreed.networkingdemo;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import np.com.ideabreed.networkingdemo.adapters.PostAdapter;
import np.com.ideabreed.networkingdemo.database.DBPostModel;
import np.com.ideabreed.networkingdemo.database.DatabaseHandler;
import np.com.ideabreed.networkingdemo.models.PostModel;
import np.com.ideabreed.networkingdemo.server.BaseClass;
import np.com.ideabreed.networkingdemo.server.WebApi;
import np.com.ideabreed.networkingdemo.utils.ConstantData;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    Button button, addnew;
    RecyclerView recyclerView;
    PostAdapter adapter;
    DatabaseHandler db;
    List<DBPostModel> postModelList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button = findViewById(R.id.getDataFromServer);
        recyclerView = findViewById(R.id.post_recycler);


        addnew = findViewById(R.id.addnewpost);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        db = new DatabaseHandler(this);

        postModelList = db.getAllPosts();

        if(postModelList.size() == 0) {
            getDataFromServer();
            postModelList = db.getAllPosts();

            adapter = new PostAdapter(MainActivity.this, postModelList);
            recyclerView.setAdapter(adapter);
        } else {
            adapter = new PostAdapter(MainActivity.this, postModelList);
            recyclerView.setAdapter(adapter);
        }


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){

                getDataFromServer();

            }
        });
        addnew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                startActivity(new Intent(MainActivity.this, PostActivity.class));
            }
        });


    }

    private void getDataFromServer(){
        if(ConstantData.checkNetwork(this)) {
            WebApi api = BaseClass.getInstance().create(WebApi.class);
            api.getAllPost().enqueue(new Callback<List<PostModel>>() {
                @Override
                public void onResponse(Call<List<PostModel>> call,
                                       Response<List<PostModel>> response){


                    if(response != null) {
                        List<PostModel> postModels = response.body();

                        DBPostModel model = new DBPostModel();

                        assert postModels != null;
                        for(int i = 0; i < postModels.size(); i++) {


                            model.setUserId(postModels.get(i).getUserId());
                            model.setTitle(postModels.get(i).getTitle());
                            model.setBody(postModels.get(i).getBody());

                            db.insertPostIntoDb(model);


                        }


                    }


                }

                @Override
                public void onFailure(Call<List<PostModel>> call, Throwable t){
                    Toast.makeText(MainActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT)
                         .show();
                }
            });


        } else {
            Toast.makeText(this, "No internet connection", Toast.LENGTH_SHORT).show();
        }

    }
}
